
let users = [
    {
        username:"UserSample1",
        email:"reader@gmail.com",
        password:"dontTalk"
    },
    {
        username:"UserSample2",
        email:"noone@gmail.com",
        password:"emptyvoid"
        
    }
];

let items = [
    {
        name:"roses",
        price:200,
        isActive:true
    },
    {
        name:"tulips",
        price:140,
        isActive:true
    }
];
// load express js module
// load in a variable called express
const express = require ("express");


// app is now server
const app = express();
const port = 4000;

// middleware
app.use(express.json())



// app.get('/',(req, res)=>{

//     res.send('this is a trial')
// });

// app.get('/users',(req, res)=>{
//     res.send(users);

// });

// app.post ('/users',(req, res)=>{
// console.log(req.body)

// let newUser = {
//     username: req.body.username,
//     email: req.body.email,
//     password: req.body.password
// }

// users.push(newUser);
// console.log(users);
// res.send(users);


// });

// app.delete('/users',(req, res)=>{
// users.pop();
// console.log(users);

// res.send(users);
// });

// // app.put('/users/:index',(req, res)=>{
// // console.log(req.body);
// // console.log(req.params);
// // let index=parseInt(req.params.index);

// // users[index].password = req.body.password;
// // res.send(users[index]);

// // });

// app.put('/users/:index', (req, res)=>{

//     let index = parseInt(req.params.index);
//     users[index].username = req.body.username;
//     res.send(users[index]);

// });

// app.get('/users/getSingleUser/:index', (req, res)=>{
//     let index=parseInt(req.params.index);
//     res.send(users[index]);

// });

/*






*/


// ACTIVITY SOLUTION

// GET ITEMS
app.get('/items',(req, res)=>{

    res.send(items)
});

// CREATE ITEMS
app.post ('/items',(req, res)=>{
    console.log(req.body)
    
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    }
    
    items.push(newItem);
    console.log(items);
    res.send(items);
    
    });
// UPDATE SINGLE ITEM
app.put('/items/:index', (req, res)=>{

        let index = parseInt(req.params.index);

        items[index].price = req.body.price;
        res.send(items[index]);
    
});



// ACTIVITY 34 SOLUTION

app.get('/items/getSingleItem/:index', (req, res)=>{
    let index=parseInt(req.params.index);
    res.send(items[index]);

});



app.put('/items/archive/:index', (req, res)=>{

    let index = parseInt(req.params.index);
    console.log(items)

    items[index].isActive = false;
    res.send(items[index]);
});




app.put('/items/activate/:index', (req, res)=>{

    let index = parseInt(req.params.index);
    console.log(items)

    items[index].isActive = true;
    res.send(items[index]);
});

app.listen(port,()=>console.log(`server is working on port: ${port}`));